#ifndef ASSIMP_REVISION_H_INC
#define ASSIMP_REVISION_H_INC

#define GitVersion 0xca191c8
#define GitBranch "master"

#endif // ASSIMP_REVISION_H_INC
