#version 330

layout(location = 0) out vec4 fragColor;

uniform sampler2D renderTexture;
uniform float time;

in vec2 UV;

void main()
{
	fragColor = texture2D(renderTexture, UV);
}