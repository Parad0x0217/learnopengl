#version 330 core

in vec2 UV;
in vec4 ShadowCoord;

layout(location = 0) out vec3 color;

uniform sampler2D myTextureSampler;
uniform sampler2D ShadowMap;

void main()
{
	vec3 LightColor = vec3(1, 1, 1);

	float bias = 0.005;
	
	vec3 MaterialDiffuseColor = texture2D(myTextureSampler, UV).rgb;

	color = texture2D(ShadowMap, ShadowCoord.xy).z * MaterialDiffuseColor * LightColor;
}