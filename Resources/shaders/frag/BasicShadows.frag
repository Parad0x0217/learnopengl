#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;
in vec4 ShadowCoord;

// Ouput data
layout(location = 0) out vec3 color;

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;
uniform sampler2D shadowMap;

void main(){

	// Light emission properties
	vec3 LightColor = vec3(1,1,1);

	float bias = 0.005;
	
	// Material properties
	vec3 MaterialDiffuseColor = texture2D( myTextureSampler, UV ).rgb;

	float visibility = 1.0;
	
	if (texture2D(shadowMap, ShadowCoord.xy).z  <  (ShadowCoord.z - bias))
	{
		visibility = 0.5;
	}

	color = visibility * MaterialDiffuseColor * LightColor;

}