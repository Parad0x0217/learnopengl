#version 450

layout (location = 0) in vec3 vertex_position; // in model space

out vec3 TexCoords;

uniform mat4 MVP;

void main()
{
	vec4 pos = MVP * vec4(vertex_position, 1.0f);
    gl_Position = pos.xyww;

    TexCoords = vertex_position;
} 