#version 330

layout(location = 0) in vec4 vertexPosition_modelSpace;

out vec2 UV;

void main()
{
	gl_Position = vertexPosition_modelSpace;
	UV = (vertexPosition_modelSpace.xy + vec2(1, 1)) / 2.0;
}