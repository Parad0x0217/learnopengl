#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "ExternalIncludes.hpp"

class Camera
{

public:

	Camera(GLFWwindow* windowObject);
	~Camera();

	void Update(double deltaTime);

	glm::mat4& getProjectionMatrix();
	glm::mat4& getViewMatrix();

	glm::vec3& getPosition();
	glm::vec3& getForward();
	glm::vec3& getUp();
	glm::vec3& getRight();

private:

	glm::mat4 ProjectionMatrix;
	glm::mat4 ViewMatrix;

	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 up;
	glm::vec3 right;

	double lastMousePositionX;
	double lastMousePositionY;

	float speed;
	float mouseSpeed;

	float horizontalAngle;
	float verticalAngle;

	GLFWwindow* window;

	char pad1;
	char pad2;
	char pad3;
	char pad4;
};

#endif