#include "Texture.hpp"


Texture::Texture(GLenum textureTarget, const std::string& file)
{
	mTextureTarget	= textureTarget;
	mFileNames.push_back(file);
}

Texture::Texture(GLenum textureTarget, const std::string& right, const std::string& left, const std::string& top, const std::string& bottom, const std::string& back, const std::string& front)
{
	mTextureTarget = textureTarget;
	mFileNames.push_back(right);
	mFileNames.push_back(left);
	mFileNames.push_back(top);
	mFileNames.push_back(bottom);
	mFileNames.push_back(back);
	mFileNames.push_back(front);
}

Texture::~Texture()
{
	glDeleteTextures(1, &mTextureObj);
}

bool Texture::Load()
{
	glGenTextures(1, &mTextureObj);
	glBindTexture(mTextureTarget, mTextureObj);

	for (unsigned int i = 0; i < mFileNames.size(); ++i)
	{
		//image format
		FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;

		//pointer to the image, once loaded
		FIBITMAP *dib = 0;

		//pointer to the image data
		BYTE* bits = 0;

		//image width and height
		unsigned int width = 0;
		unsigned int height = 0;

		//check if there is transparency or not
		FREE_IMAGE_COLOR_TYPE type;

		//format of image
		GLenum glFormat;
		GLenum glInternalFormat;

		//check the file signature and deduce its format
		fif = FreeImage_GetFileType(mFileNames.at(i).c_str(), 0);

		//if still unknown, try to guess the file format from the file extension
		if (fif == FIF_UNKNOWN)
			fif = FreeImage_GetFIFFromFilename(mFileNames.at(i).c_str());

		//if still unknown, return failure
		if (fif == FIF_UNKNOWN)
			return false;

		//check that the plugin has reading capabilities and load the file
		if (FreeImage_FIFSupportsReading(fif))
			dib = FreeImage_Load(fif, mFileNames.at(i).c_str());

		//Flip because OpenGL is weird
		//FreeImage_FlipVertical(dib);

		//TODO: why do this?
		dib = FreeImage_ConvertTo24Bits(dib);

		//if the image failed to load, return failure
		if (!dib)
			return false;

		//retrieve the image data
		bits = FreeImage_GetBits(dib);

		//get the image width and height
		width = FreeImage_GetWidth(dib);
		height = FreeImage_GetHeight(dib);

		//get image color type
		type = FreeImage_GetColorType(dib);

		//if this somehow one of these failed (they shouldn't), return failure
		if ((bits == 0) || (width == 0) || (height == 0))
			return false;

		switch (type)
		{
		case FIC_RGB:
			glInternalFormat = GL_RGB;
			glFormat = GL_BGR;
			break;

		case FIC_RGBALPHA:
			glInternalFormat = GL_RGBA;
			glFormat = GL_BGRA;
			break;

		case FIC_MINISWHITE:
		case FIC_MINISBLACK:
		case FIC_PALETTE:
		case FIC_CMYK:
		default:
			glInternalFormat = GL_RGBA;
			glFormat = GL_RGBA;
		}
		

		switch (mTextureTarget)
		{
		case GL_TEXTURE_2D:

			//Should this be GL_BGRA?
			glTexImage2D(mTextureTarget, 0, (GLint)glInternalFormat, (GLsizei)width, (GLsizei)height, 0, glFormat, GL_UNSIGNED_BYTE, bits);

			break;
		case GL_TEXTURE_CUBE_MAP:

			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, (GLint)glInternalFormat, (GLsizei)width, (GLsizei)height, 0, glFormat, GL_UNSIGNED_BYTE, bits);
			//glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, glInternalFormat, width, height, 0, glFormat, GL_UNSIGNED_BYTE, bits);
			//glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, glInternalFormat, width, height, 0, glFormat, GL_UNSIGNED_BYTE, bits);
			//glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, glInternalFormat, width, height, 0, glFormat, GL_UNSIGNED_BYTE, bits);
			//glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, glInternalFormat, width, height, 0, glFormat, GL_UNSIGNED_BYTE, bits);
			//glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, glInternalFormat, width, height, 0, glFormat, GL_UNSIGNED_BYTE, bits);

			break;
		}

		glTexParameteri(mTextureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(mTextureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(mTextureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(mTextureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(mTextureTarget, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		//Free FreeImage's copy of the data
		FreeImage_Unload(dib);
	}

	glBindTexture(mTextureTarget, NULL);

	return true;
}

void Texture::Bind()
{
	glBindTexture(mTextureTarget, mTextureObj);
}

void Texture::Unbind()
{
	glBindTexture(mTextureTarget, 0);
}