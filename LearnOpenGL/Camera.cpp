#include "Camera.hpp"

Camera::Camera(GLFWwindow* windowObject)
{
	window = windowObject;

	speed = 128.0f;
	mouseSpeed = 0.0025f;

	position = glm::vec3(0, 0, -10); 
	horizontalAngle = 0.0f;
	verticalAngle = 0.0f;

	ProjectionMatrix = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 4096.0f);
}

Camera::~Camera()
{

}

void Camera::Update(double deltaTime)
{
	// Get mouse position
	double xpos = lastMousePositionX;
	double ypos = lastMousePositionY;

	if (glfwGetWindowAttrib(window, GLFW_FOCUSED))
	{
		glfwGetCursorPos(window, &xpos, &ypos);
		lastMousePositionX = xpos;
		lastMousePositionY = ypos;
		// Reset mouse position for next frame
		glfwSetCursorPos(window, 1024 / 2, 768 / 2);
	}

	// Compute new orientation
	horizontalAngle += mouseSpeed * float(1024/2 - xpos );
	verticalAngle   += mouseSpeed * float( 768/2 - ypos );

	// Limit rotation so camera doesn't turn upside-down
	verticalAngle = glm::clamp(verticalAngle, -1.57079633f, 1.57079633f);

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	forward = glm::vec3(cos(verticalAngle) * sin(horizontalAngle), sin(verticalAngle), cos(verticalAngle) * cos(horizontalAngle));

	// Right vector
	right = glm::vec3( sin(horizontalAngle - 1.57079633f), 0, cos(horizontalAngle - 1.57079633f) );

	// Up vector
	up = glm::cross( right, forward );

	if (glfwGetKey(window, 'W'))
		position += forward * (float)deltaTime * speed;

	if (glfwGetKey(window, 'S'))
		position -= forward * (float)deltaTime * speed;

	if (glfwGetKey(window, 'A'))
		position -= right * (float)deltaTime * speed;

	if (glfwGetKey(window, 'D'))
		position += right * (float)deltaTime * speed;

	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT))
		speed = 64.0f;
	else
		speed = 32.0f;

	ViewMatrix = glm::lookAt(position, position+forward, up);
}

glm::mat4& Camera::getProjectionMatrix()
{
	return ProjectionMatrix;
}

glm::mat4& Camera::getViewMatrix()
{
	return ViewMatrix;
}

glm::vec3& Camera::getPosition()
{
	return position;
}

glm::vec3& Camera::getForward()
{
	return forward;
}

glm::vec3& Camera::getUp()
{
	return up;
}

glm::vec3& Camera::getRight()
{
	return right;
}
