#pragma once

#include "GameObject.h"

#include "ExternalIncludes.hpp"

class Camera; // TODO: Replace with camera manager or something similar. Dependency inject?
class Texture;


class Torus : public GameObject
{
public:

	virtual ~Torus() override;

	// TODO: REMOVE THESE
	Camera* camera;
	double time;
	// TODO

	virtual void Start() override;
	virtual void Update() override;

	virtual void Draw() override;

private:

	Texture* testTexture;

	GLint AmbientShaderMVPMatrixLocation;
	GLint AmbientShaderTextureLocation;

	GLuint AmbientShaderID;
	GLuint TorusVAO;
	GLuint vertexbuffer;
	GLuint uvbuffer;
	GLuint normalbuffer;


	unsigned int numVertices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

};