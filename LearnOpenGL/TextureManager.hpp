#ifndef TEXTURE_MANAGER_HPP
#define TEXTURE_MANAGER_HPP

#include <map>
#include <memory>

#include "Texture.hpp"

class TextureManager
{

public:

	static TextureManager& getInstance()
	{
		static TextureManager instance;

		return instance;
	}

	std::shared_ptr<Texture> loadTexture(GLuint textureTarget, const std::string& File);

private:

	TextureManager();
	~TextureManager();

	TextureManager(TextureManager const&);
	void operator=(TextureManager const&);

	std::map<std::string, std::shared_ptr<Texture>> loadedTextures;

};

#endif