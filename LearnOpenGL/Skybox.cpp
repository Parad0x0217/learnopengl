#include "Skybox.hpp"

#include "ExternalIncludes.hpp"

#include "Camera.hpp"
#include "Shader.hpp"
#include "Texture.hpp"

static const GLfloat skyboxVertices[] = {
	// Positions          
	-1024.0f,  1024.0f, -1024.0f,
	-1024.0f, -1024.0f, -1024.0f,
	1024.0f, -1024.0f, -1024.0f,
	1024.0f, -1024.0f, -1024.0f,
	1024.0f,  1024.0f, -1024.0f,
	-1024.0f,  1024.0f, -1024.0f,

	-1024.0f, -1024.0f,  1024.0f,
	-1024.0f, -1024.0f, -1024.0f,
	-1024.0f,  1024.0f, -1024.0f,
	-1024.0f,  1024.0f, -1024.0f,
	-1024.0f,  1024.0f,  1024.0f,
	-1024.0f, -1024.0f,  1024.0f,

	1024.0f, -1024.0f, -1024.0f,
	1024.0f, -1024.0f,  1024.0f,
	1024.0f,  1024.0f,  1024.0f,
	1024.0f,  1024.0f,  1024.0f,
	1024.0f,  1024.0f, -1024.0f,
	1024.0f, -1024.0f, -1024.0f,

	-1024.0f, -1024.0f,  1024.0f,
	-1024.0f,  1024.0f,  1024.0f,
	1024.0f,  1024.0f,  1024.0f,
	1024.0f,  1024.0f,  1024.0f,
	1024.0f, -1024.0f,  1024.0f,
	-1024.0f, -1024.0f,  1024.0f,

	-1024.0f,  1024.0f, -1024.0f,
	1024.0f,  1024.0f, -1024.0f,
	1024.0f,  1024.0f,  1024.0f,
	1024.0f,  1024.0f,  1024.0f,
	-1024.0f,  1024.0f,  1024.0f,
	-1024.0f,  1024.0f, -1024.0f,

	-1024.0f, -1024.0f, -1024.0f,
	-1024.0f, -1024.0f,  1024.0f,
	1024.0f, -1024.0f, -1024.0f,
	1024.0f, -1024.0f, -1024.0f,
	-1024.0f, -1024.0f,  1024.0f,
	1024.0f, -1024.0f,  1024.0f
};

void Skybox::Start()
{
	skyboxShaderProgramId = LoadShaders("../resources/shaders/vert/skybox.vert", "../resources/shaders/frag/skybox.frag");

	skyboxShaderMVPMatrixLocation = glGetUniformLocation(skyboxShaderProgramId, "MVP");
	skyboxShaderTextureLocation = glGetUniformLocation(skyboxShaderProgramId, "skybox");

	glGenVertexArrays(1, &skyboxVAO);
	glBindVertexArray(skyboxVAO);

	glGenBuffers(1, &skyboxVBO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	skyboxTexture = new Texture(GL_TEXTURE_CUBE_MAP, "../resources/textures/mp_drakeq/drakeq_rt.tga", "../resources/textures/mp_drakeq/drakeq_lf.tga",
		"../resources/textures/swirl.png", "../resources/textures/mp_drakeq/drakeq_up.tga",
		"../resources/textures/mp_drakeq/drakeq_bk.tga", "../resources/textures/mp_drakeq/drakeq_ft.tga");
	skyboxTexture->Load();
}


void Skybox::Update()
{
	// T * R * S
	glm::mat4 translation = glm::translate(camera->getPosition());
	glm::mat4 rotation = glm::mat4();
	glm::mat4 scale = glm::mat4();

	transform = translation * rotation * scale;
}

void Skybox::Draw()
{
	glm::mat4 MVP = camera->getProjectionMatrix() * camera->getViewMatrix() * transform;


	glDepthFunc(GL_LEQUAL);
	glUseProgram(skyboxShaderProgramId);
	glUniformMatrix4fv(skyboxShaderMVPMatrixLocation, 1, GL_FALSE, &MVP[0][0]);
	glActiveTexture(GL_TEXTURE2);
	skyboxTexture->Bind();
	glUniform1i(skyboxShaderTextureLocation, 2);
	glBindVertexArray(skyboxVAO);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);
	glDisableVertexAttribArray(0);
	skyboxTexture->Unbind();
}

