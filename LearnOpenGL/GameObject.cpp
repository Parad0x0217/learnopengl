#include "GameObject.h"
#include "GameObjectManager.h"

GameObject::GameObject()
{
	GameObjectManager::GetInstance().RegisterGameObject(this);
}

GameObject::~GameObject()
{
	GameObjectManager::GetInstance().DeregisterGameObject(this);
}

void GameObject::Start()
{
}

void GameObject::Update()
{

}

void GameObject::Draw()
{
}
