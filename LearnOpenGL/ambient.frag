#version 450

in vec2 UV;

layout (location = 0) out vec4 FragColor;

uniform sampler2D TextureSampler;

void main()
{
	FragColor = texture(TextureSampler, UV) * vec4(0.5f, 0.5f, 0.5f, 1.0f);
}