#ifndef GAMEOBJECT_MANAGER
#define GAMEOBJECT_MANAGER

#include "ExternalIncludes.hpp"

class GameObject;

class GameObjectManager
{
public:

	static GameObjectManager& GetInstance();

	void RegisterGameObject(GameObject* gameObject);
	void DeregisterGameObject(GameObject* gameObject);

	void Start();
	void Update();
	void Draw();

	GameObjectManager(GameObjectManager const&) = delete;
	void operator=(GameObjectManager const&) = delete;

private:

	GameObjectManager();

	std::vector<GameObject*> gameObjects;

};

#endif