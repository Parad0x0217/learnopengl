#include "Cube.h"

#include "Camera.hpp"
#include "Shader.hpp"
#include "Texture.hpp"

Cube::~Cube()
{
}

void Cube::Start()
{
	std::cout << "Compiling and linking ambient shaders..." << std::endl;
	AmbientShaderID = LoadShaders("../resources/shaders/vert/ambient.vert", "../resources/shaders/frag/ambient.frag");

	testTexture = new Texture(GL_TEXTURE_2D, "../resources/textures/uvtemplate.tga");
	testTexture->Load();

	AmbientShaderMVPMatrixLocation = glGetUniformLocation(AmbientShaderID, "MVP");
	AmbientShaderTextureLocation = glGetUniformLocation(AmbientShaderID, "TextureSampler");

	glGenVertexArrays(1, &cubeVAO);
	glBindVertexArray(cubeVAO);
	std::cout << sizeof(g_vertex_buffer_data);
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_uv_buffer_data), g_uv_buffer_data, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

}

void Cube::Update()
{
	// T * R * S
	glm::mat4 translation = glm::translate(glm::vec3(glm::fastCos(time * 0.5f) * 4.0f, glm::fastSin(time * 0.5f) * 4.0f, 0.0f));
	glm::mat4 rotation = glm::rotate((float)time, glm::vec3(2.0f, 1.0f, 1.5f));
	glm::mat4 scale = glm::scale(glm::vec3(glm::fastSin(time) + 2.0f, 1.0f, 1.0f));

	transform = translation * rotation * scale;
}

void Cube::Draw()
{
	glm::mat4 MVP = camera->getProjectionMatrix() * camera->getViewMatrix() * transform;


	glDepthFunc(GL_LESS);

	glUseProgram(AmbientShaderID);
	glUniformMatrix4fv(AmbientShaderMVPMatrixLocation, 1, GL_FALSE, &MVP[0][0]);

	glBindVertexArray(cubeVAO);

	glActiveTexture(GL_TEXTURE17);
	testTexture->Bind();
	glUniform1i(AmbientShaderTextureLocation, 17);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);

	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	testTexture->Unbind();
}
