#include "GameObjectManager.h"
#include "GameObject.h"

GameObjectManager::GameObjectManager()
{
}

GameObjectManager& GameObjectManager::GetInstance()
{
	static GameObjectManager instance;
	return instance;
}

void GameObjectManager::RegisterGameObject(GameObject* gameObject)
{
	gameObjects.push_back(gameObject);
}

void GameObjectManager::DeregisterGameObject(GameObject* gameObject)
{
	// TODO: Maybe just make this wait until update/draw happens, then read from flag to remove
	gameObjects.erase(std::find(std::begin(gameObjects), std::end(gameObjects), gameObject));
}

void GameObjectManager::Start()
{
	std::for_each(std::begin(gameObjects), std::end(gameObjects), [](GameObject* gameObject) {
		gameObject->Start();
	});
}

void GameObjectManager::Update()
{
	std::for_each(std::begin(gameObjects), std::end(gameObjects), [](GameObject* gameObject) {
		gameObject->Update();
	});
}

void GameObjectManager::Draw()
{
	std::for_each(std::begin(gameObjects), std::end(gameObjects), [](GameObject* gameObject) {
		gameObject->Draw();
	});
}
