#ifndef GAMEOBJECT
#define GAMEOBJECT

#include "ExternalIncludes.hpp"

class GameObject
{
public:

	GameObject();
	virtual ~GameObject();

	virtual void Start();
	virtual void Update();

	// TODO: make less visible? possible for private?
	virtual void Draw();

protected:

	//AddComponent(Component* component);

	glm::mat4 transform; // Translation, Rotation, Scale

private:

	
	//std::vector<Component*> components;

};

#endif