#include "Torus.h"

#include "Camera.hpp"
#include "Shader.hpp"
#include "Texture.hpp"

Torus::~Torus()
{
}

void Torus::Start()
{
	std::cout << "Compiling and linking ambient shaders..." << std::endl;
	AmbientShaderID = LoadShaders("../resources/shaders/vert/ambient.vert", "../resources/shaders/frag/ambient.frag");

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile("../resources/models/t.obj", aiPostProcessSteps::aiProcess_Triangulate);

	// Just grab the first mesh for now
	aiMesh* mesh = scene->mMeshes[0];
	aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
	numVertices = mesh->mNumVertices;
	for (unsigned int i = 0; i < numVertices; ++i)
	{
		aiVector3D vert = mesh->mVertices[i];
		aiVector3D uv = mesh->mTextureCoords[0][i]; // grab first UV channel for now
		aiVector3D norm = mesh->mNormals[i];
		vertices.push_back(glm::vec3(vert.x, vert.y, vert.z));
		uvs.push_back(glm::vec2(uv.x, uv.y));
		normals.push_back(glm::vec3(norm.x, norm.y, norm.z));
	}

	testTexture = new Texture(GL_TEXTURE_2D, "../resources/textures/t.png");
	testTexture->Load();

	AmbientShaderMVPMatrixLocation = glGetUniformLocation(AmbientShaderID, "MVP");
	AmbientShaderTextureLocation = glGetUniformLocation(AmbientShaderID, "TextureSampler");

	glGenVertexArrays(1, &TorusVAO);
	glBindVertexArray(TorusVAO);

	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
}

void Torus::Update()
{
	// T * R * S
	glm::mat4 translation;// = glm::translate(glm::vec3(glm::fastCos(time * 0.5f) * 4.0f, glm::fastSin(time * 0.5f) * 4.0f, 0.0f));
	glm::mat4 rotation = glm::rotate((float)time * 0.125f, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 scale;// = glm::scale(glm::vec3(2.0f, 2.0f, 2.0f));

	transform = translation * rotation * scale;
}

void Torus::Draw()
{
	glm::mat4 MVP = camera->getProjectionMatrix() * camera->getViewMatrix() * transform;


	glDepthFunc(GL_LESS);

	glUseProgram(AmbientShaderID);
	glUniformMatrix4fv(AmbientShaderMVPMatrixLocation, 1, GL_FALSE, &MVP[0][0]);

	glBindVertexArray(TorusVAO);

	glActiveTexture(GL_TEXTURE17);
	testTexture->Bind();
	glUniform1i(AmbientShaderTextureLocation, 17);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);

	glDrawArrays(GL_TRIANGLES, 0, numVertices);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);

	testTexture->Unbind();
}
