#include "TextureManager.hpp"

TextureManager::TextureManager()
{

}

TextureManager::~TextureManager()
{
	std::map<std::string, std::shared_ptr<Texture>>::iterator i;
	for (i = loadedTextures.begin(); i != loadedTextures.end(); ++i)
	{
		if (i->second.unique())
		{
			printf("deleting %s\n", i->first.c_str());
			i->second.reset();	
		}
	}
}

std::shared_ptr<Texture> TextureManager::loadTexture(GLuint textureTarget, const std::string& file)
{
	std::map<std::string, std::shared_ptr<Texture>>::iterator i = loadedTextures.lower_bound(file);

	if (i != loadedTextures.end() && !(loadedTextures.key_comp()(file, i->first)))
	{
		printf("ALREADY EXISTS: %s\n", file.c_str());
		return i->second;
	}

	// does not exist in the map
	// load texture and add it to the map
	std::shared_ptr<Texture> t = std::make_shared<Texture>(textureTarget, file);

	if (t->Load())
	{
		printf("DOESNT EXIST: %s\n", file.c_str());
		loadedTextures.insert(i, std::map<std::string, std::shared_ptr<Texture>>::value_type(file, t));
	}
	else
	{
		printf("ERROR NOT FOUND: %s\n", file.c_str());
		return 0;
	}

	return t;
}
