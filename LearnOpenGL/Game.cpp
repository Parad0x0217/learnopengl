#include "Game.hpp"

#include "ExternalIncludes.hpp"

#include "GameObjectManager.h"

Game::Game() : camera(nullptr)
{

}

Game::~Game()
{

}

//Update to using namespace GLFWAPI to allow usage of keys without namespace identifier
bool Game::Init(std::string Title)
{
	// Initialize GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return false;
	}

	glfwWindowHint(GLFW_SAMPLES, 0); //FSAA Samples
	glfwWindowHint(GLFW_RESIZABLE, false);

	window = glfwCreateWindow(1024, 768, Title.c_str(), NULL, NULL);

	if (!window)
	{
		fprintf(stderr, "Failed to open GLFW window\n");

		glfwTerminate();

		return false;
	}

	std::cout << "Using OpenGL "
		<< glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR)
		<< "."
		<< glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR)
		<< std::endl << std::endl;

	glfwSwapInterval(1); //turn on vsync

	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		return false;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glDepthMask(GL_TRUE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	camera = new Camera(window);

	torus = new Torus();
	torus->camera = camera;

	cube = new Cube();
	cube->camera = camera;

	skybox = new Skybox();
	skybox->camera = camera;

	glBindVertexArray(NULL);

	return true;
}

void Game::Update()
{
	previousTime = currentTime;
	currentTime = glfwGetTime();
	deltaTime = currentTime - previousTime;

	glfwPollEvents();

	torus->time = currentTime;
	cube->time = currentTime;
	skybox->time = currentTime;
	
	char time[256];
	sprintf(time, "%f", currentTime);
	glfwSetWindowTitle(window, time);
	GameObjectManager::GetInstance().Update();

	camera->Update(deltaTime);
	
}

void Game::Draw()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 1024, 768);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GameObjectManager::GetInstance().Draw();

	glBindVertexArray(NULL);

	// Swap buffers
	glfwSwapBuffers(window);
}

void Game::Go()
{
	GameObjectManager::GetInstance().Start();

	//handle states here. playing, pause, menu, etc
	//right now just quit if user presses escape key
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(window))
	{
		Update();
		Draw();

		GLenum error = glGetError();
		if (error != GL_NO_ERROR)
		{
			std::cout << "OpenGL Error: " << error << std::endl;
		}
	}
}

void Game::Cleanup()
{
	glfwDestroyWindow(window);
	glfwTerminate();

	delete camera;
	camera = nullptr;
}