#include "Game.hpp"

int main()
{
	Game myGame = Game();

	if (myGame.Init("LearnOpenGL"))
	{
		myGame.Go();
	}

	myGame.Cleanup();

	return 0;
}