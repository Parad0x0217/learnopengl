#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "ExternalIncludes.hpp"

class Texture
{

public:

	Texture(GLenum textureTarget, const std::string& file);

	Texture(GLenum textureTarget, const std::string& top, const std::string& bottom, const std::string& left, const std::string& right, const std::string& front, const std::string& back);

	~Texture();

	bool Load();

	void Bind();
	void Unbind();

private:

	std::vector<std::string> mFileNames;

	GLenum mTextureTarget;
	GLuint mTextureObj;
};

#endif