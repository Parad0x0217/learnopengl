#ifndef SKYBOX_HPP
#define SKYBOX_HPP

#include "ExternalIncludes.hpp"

#include "GameObject.h"

class Camera; // TODO: Replace with camera manager or something similar. Dependency inject?
class Texture;

class Skybox : public GameObject
{
public:

	// TODO: REMOVE THESE
	Camera* camera;
	double time;
	// TODO

	virtual void Start() override;
	virtual void Update() override;

	virtual void Draw() override;

private:

	GLuint skyboxShaderProgramId;
	GLuint skyboxVAO;
	GLuint skyboxVBO;
	Texture* skyboxTexture;
	GLint skyboxShaderMVPMatrixLocation;
	GLint skyboxShaderTextureLocation;

};

#endif