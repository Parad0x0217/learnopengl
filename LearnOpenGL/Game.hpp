#ifndef GAME_HPP
#define GAME_HPP

// TODO : http://stackoverflow.com/questions/2541984/how-to-suppress-warnings-in-external-headers-in-visual-c

#include "Camera.hpp"

#include "Cube.h"
#include "Skybox.hpp"
#include "Torus.h"

#include "Shader.hpp"

#include "TextureManager.hpp"
#include "Texture.hpp"

struct GLFWwindow;

class Game
{

public:

	Game();
	~Game();

	bool Init(std::string Title);
	void Go();
	void Cleanup();

private:

	GLFWwindow* window;
	Camera* camera;

	Torus* torus;
	Cube* cube;
	Skybox* skybox;

	double currentTime;
	double previousTime;
	double deltaTime;

	void Update();
	void Draw();

};

#endif